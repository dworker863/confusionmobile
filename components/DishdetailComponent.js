import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, Modal, Button, StyleSheet, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Rating, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';

const mapStateToProps = state => {
  return {
    dishes: state.dishes,
    comments: state.comments,
    favorites: state.favorites
  }
}

const mapDispatchToProps = dispatch => ({
  postFavorite: (dishId) => dispatch(postFavorite(dishId)),
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
});

function RenderDish(props) {
  const dish = props.dish;

  let view;

  const handleViewRef = ref => view = ref;

  const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
    if (dx < -200)
      return true;
    else
      return false;
  };

  const recognizeComment = ({ moveX, moveY, dx, dy }) => {
    if (dx > 200)
      return true;
    else
      return false;
  };

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (e, gestureState) => {
      return true;
    },
    onPanResponderGrant: () => {
      view.rubberBand(1000)
      .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
    },
    onPanResponderEnd: (e, gestureState) => {
      if (recognizeDrag(gestureState)) {
        Alert.alert(
          'Add to Favorite',
          'Are you sure you wish to add ' + dish.name + ' to your favorites?',
          [
            {
              text: 'Cancel',
              onPress: () => console.log('Cancel pressed'),
              style: 'cancel'
            },
            {
              text: 'OK',
              onPress: () => props.favorite ? console.log('Alredy favorite') : props.onPress()
            },
          ],
          { cancelable: false }
        )
      }
      if (recognizeComment(gestureState)) {
        props.toggleModal();
      }

      return true;
    }
  });

  const shareDish = (title, message, url) => {
    Share.share({
      title: title,
      message: `${title}: ${message} ${url}`,
      url: url
    }, {
      dialogTitle: 'Share ' + title
    });
  }

  if (dish != null) {
    return (
      <Animatable.View
        animation="fadeInDown"
        duration={2000}
        delay={1000}
        ref={handleViewRef}
        {...panResponder.panHandlers}
      >
        <Card>
          <Card.Title>{dish.name}</Card.Title>
          <Card.Image source={{ uri: baseUrl + dish.image }} />
          <Text style={{ margin: 10 }}>
            {dish.description}
          </Text>
          <View style={styles.iconsWrapper}>
            <Icon
              raised
              reverse
              name={props.favorite ? 'heart' : 'heart-o'}
              type="font-awesome"
              color="#f50"
              onPress={() => props.favorite ? console.log('Alredy favorite') : props.onPress()}
            />
            <Icon
              raised
              reverse
              name='pencil'
              type='font-awesome'
              color="#512DA8"
              onPress={() => props.toggleModal()}
            />
            <Icon
              raised
              reverse
              name='share'
              type='font-awesome'
              color='#51D2A8'
              onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)}              
            />
          </View>
        </Card>
      </Animatable.View>
    );
  }
  else {
    return <View></View>
  }
}

function RenderComments(props) {
  const comments = props.comments;

  const RenderCommentsItem = ({ item, index }) => {
    return (
      <View key={index} style={{ margin: 10 }}>
        <Text style={{ fontSize: 14 }}>{item.comment}</Text>
        <Rating
          imageSize={10}
          readonly
          startingValue={item.rating}
          style={styles.rating}
        />
        <Text style={{ fontSize: 12 }}>{`-- ${item.rating}, ${item.date}`}</Text>
      </View>
    );
  }

  return (
    <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
      <Card>
        <Card.Title>Comments</Card.Title>
        <FlatList
          data={comments}
          renderItem={RenderCommentsItem}
          keyExtractor={item => item.id.toString()}
        />
      </Card>
    </Animatable.View>
  );
}

class DishDetail extends Component {
  constructor(props) {
    super(props);

    this.toggleModal = this.toggleModal.bind(this);

    this.state = {
      rating: 5,
      author: '',
      comment: '',
      showModal: false
    }
  }

  markFavorite(dishId) {
    this.props.postFavorite(dishId)
  }

  static navigationOptions = {
    title: 'Dish Details'
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
  }

  resetForm() {
    this.setState({
      rating: 5,
      author: '',
      comment: '',
      showModal: false
    });
  }

  handleComment(dishId, rating, author, comment) {
    this.props.postComment(dishId, rating, author, comment);
    this.toggleModal();
    this.resetForm();
  }

  render() {
    const dishId = this.props.route.params.dishId;

    return (
      <ScrollView>
        <View>
          <Modal
            animationType={'slide'}
            transparent={false}
            visible={this.state.showModal}
            onDismiss={() => { this.toggleModal(); this.resetForm() }}
            onRequestClose={() => { this.toggleModal(); this.resetForm() }}
          >
            <Rating
              showRating
              fractions={0}
              startingValue={5}
              onFinishRating={rating => this.setState({ rating: rating })}
            />
            <Input
              placeholder='Author'
              leftIcon={{ type: 'font-awesome', name: 'user-o' }}
              value={this.state.value}
              onChangeText={text => this.setState({ author: text })}
            />
            <Input
              placeholder='Author'
              leftIcon={{ type: 'font-awesome', name: 'comment-o' }}
              value={this.state.value}
              onChangeText={text => this.setState({ comment: text })}
            />
            <View style={styles.btn}>
              <Button
                title='submit'
                color='#512DA8'
                style={styles.btn}
                onPress={() => this.handleComment(dishId, this.state.rating, this.state.author, this.state.comment)}
              />
            </View>
            <View style={styles.btn}>
              <Button
                title='cancel'
                color='grey'
                onPress={() => this.toggleModal()}
              />
            </View>
          </Modal>
        </View>
        <RenderDish
          dish={this.props.dishes.dishes[+dishId]}
          favorite={this.props.favorites.some(el => el === dishId)}
          onPress={() => this.markFavorite(dishId)}
          toggleModal={() => this.toggleModal()}
        />
        <RenderComments comments={this.props.comments.comments.filter(comment => comment.dishId === dishId)} />
      </ScrollView>

    );
  }
}

const styles = StyleSheet.create({
  rating: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  iconsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  btn: {
    marginBottom: 30
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(DishDetail);