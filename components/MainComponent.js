import React, { Component } from 'react';
import Menu from './MenuComponent';
import Home from './HomeComponent';
import DishDetail from './DishdetailComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import Reservation from './ReservationComponent';
import Favorites from './FavoriteComponent';
import Login from './LoginComponent';
import { View, Platform, Image, Text, StyleSheet, SafeAreaView, ScrollView, ToastAndroid } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchDishes, fetchComments, fetchLeaders, fetchPromos } from '../redux/ActionCreators';
import NetInfo from '@react-native-community/netinfo';

const mapStateToProps = state => {
  return {

  }
}

const mapDispatchToProps = dispatch => ({
  fetchDishes: () => dispatch(fetchDishes()),
  fetchComments: () => dispatch(fetchComments()),
  fetchPromos: () => dispatch(fetchPromos()),
  fetchLeaders: () => dispatch(fetchLeaders())
});

const MenuNavigator = createStackNavigator();
const HomeNavigator = createStackNavigator();
const ContactNavigator = createStackNavigator();
const AboutNavigator = createStackNavigator();
const ReservationNavigator = createStackNavigator();
const FavoritesNavigator = createStackNavigator();
const LoginNavigator = createStackNavigator();
const MainNavigator = createDrawerNavigator();

function MenuNavigation() {
  return (
    <MenuNavigator.Navigator
      initialRouteName='Menu'
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <MenuNavigator.Screen
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
        name="Menu"
        component={Menu}
      />
      <MenuNavigator.Screen name="Dishdetail" component={DishDetail} />
    </MenuNavigator.Navigator>
  );
}

function HomeNavigation() {
  return (
    <HomeNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <HomeNavigator.Screen
        name="Home"
        component={Home}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </HomeNavigator.Navigator>
  );
}

function ContactNavigation() {
  return (
    <ContactNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <ContactNavigator.Screen
        name="Contact"
        component={Contact}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </ContactNavigator.Navigator>
  );
}

function AboutNavigation() {
  return (
    <ContactNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <ContactNavigator.Screen
        name="About"
        component={About}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </ContactNavigator.Navigator>
  );
}

function ReservationNavigation() {
  return (
    <ReservationNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <ReservationNavigator.Screen
        name="Reserve Table"
        component={Reservation}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </ReservationNavigator.Navigator>
  );
}

function FavoritesNavigation() {
  return (
    <FavoritesNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <FavoritesNavigator.Screen
        name="My Favorites"
        component={Favorites}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </FavoritesNavigator.Navigator>
  );
}

function LoginNavigation() {
  return (
    <LoginNavigator.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#512DA8'
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          color: '#fff'
        }
      }}
    >
      <LoginNavigator.Screen
        name="Login"
        component={Login}
        options={({ navigation }) => ({
          headerLeft: () => <Icon name="menu" size={24} color="white" onPress={() => navigation.toggleDrawer()} />
        })}
      />
    </LoginNavigator.Navigator>
  );
}

const CustomDrawerContentComponent = (props) => (
  <ScrollView>
    <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
      <View style={styles.drawerHeader}>
        <View style={{ flex: 1 }}>
          <Image source={require('./images/logo.png')} style={styles.drawerImage} />
        </View>
        <View style={{ flex: 2 }}>
          <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
        </View>
      </View>
      <DrawerItemList {...props} />
    </SafeAreaView>
  </ScrollView>
)

function MainNavigation() {
  return (
    <MainNavigator.Navigator
      drawerStyle={{ backgroundColor: '#D1C4E9' }}
      initialRouteName='Home'
      drawerContent={props => <CustomDrawerContentComponent {...props} />}
    >
      <MainNavigator.Screen
        name="Login"
        component={LoginNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="sign-in" type="font-awesome" size={24} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="Home"
        component={HomeNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="home" type="font-awesome" size={24} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="About"
        component={AboutNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="info-circle" type="font-awesome" size={24} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="Menu"
        component={MenuNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="list" type="font-awesome" size={24} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="Contact"
        component={ContactNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="address-card" type="font-awesome" size={22} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="My Favorites"
        component={FavoritesNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="heart" type="font-awesome" size={24} color={tintColor} />
        }}
      />
      <MainNavigator.Screen
        name="Reserve Table"
        component={ReservationNavigation}
        options={{
          drawerIcon: ({ tintColor }) => <Icon name="cutlery" type="font-awesome" size={24} color={tintColor} />
        }}
      />
    </MainNavigator.Navigator>
  );
}


class Main extends Component {

  componentWillMount() {
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromos();
    this.props.fetchLeaders();

    NetInfo.fetch()
      .then((connectionInfo) => {
        ToastAndroid.show(`Initial Network Connectivity Type: ${connectionInfo.type}`, ToastAndroid.LONG);
      });

    NetInfo.addEventListener(connectionChange => this.handleConnectivityChange(connectionChange));
  }

  componentWillUnmount() {
    NetInfo.removeEventListener(connectionChange => this.handleConnectivityChange(connectionChange))
  }

  handleConnectivityChange = (connectionInfo) => {
    switch (connectionInfo.type) {
      case 'none':
        ToastAndroid.show('Your are now offline', ToastAndroid.LONG);
        break;

      case 'wifi':
        ToastAndroid.show('You are now on WiFi', ToastAndroid.LONG);
        break;

      case 'cellular':
        ToastAndroid.show('Your are now conected to Cellualar!', ToastAndroid.LONG);
        break;

      case 'unknown':
        ToastAndroid.show('You now have unknown connection!', ToastAndroid.LONG);
        break;

      default:
        break;
    }
  }

  render() {
    return (
      <NavigationContainer>
        <MainNavigation />
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  drawerHeader: {
    backgroundColor: '#512DA8',
    height: 140,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row'
  },
  drawerHeaderText: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold'
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Main);